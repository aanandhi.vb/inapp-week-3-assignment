# InApp Week - 3 Assignment
# Author      : Aanandhi V B
# Date        : November 19, 2020
# Description : A program to implement the Virtual Pet Game
# Project URL : https://gitlab.com/aanandhi.vb/inapp-week-3-assignment/-/blob/master/virtual_pet_game.py

from random import randrange
import random

class Pet:

    boredom_threshold = 10
    hunger_threshold = 10
    boredom_decrement = 4
    hunger_decrement = 4
    sounds = []

    def __init__(self, name):

        self.name = name
        self.hunger = randrange(0, self.hunger_threshold)
        self.boredom = randrange(0, self.boredom_threshold)
        self.sounds = self.sounds[:]

    def __str__(self):

        print("Current status of {} - Hunger: {}, Boredom: {}".format(self.name, self.hunger, self.boredom))
        if self.hunger > self.hunger_threshold:
            print(self.name + " is feeling hungry!")
        elif self.boredom > self.boredom_threshold:
            print(self.name + " is feeling bored!")
        elif self.hunger <= self.hunger_threshold and self.boredom <= self.boredom_threshold:
            print(self.name + " is feeling happy!")

    def clock_tick(self):
        self.boredom += 1
        self.hunger += 1

    def hi(self):
        if len(self.sounds) > 0:
            print("{} says: {}".format(self.name, random.choice(self.sounds)))
        else:
            print("Please teach me some words!")
        self.reduce_boredom()

    def teach(self, new_word):
        self.sounds.append(new_word)
        print("New sound added!")
        self.reduce_boredom()

    def feed(self):
        print("{} is fed!".format(self.name))
        self.reduce_hunger()

    def reduce_hunger(self):
        self.hunger = self.hunger - self.hunger_decrement if (self.hunger - self.hunger_decrement) >= 0 else self.hunger

    def reduce_boredom(self):
        self.boredom = self.boredom - self.boredom_decrement if (self.boredom - self.boredom_decrement) >= 0 else self.boredom


all_pets = [Pet("Cat"), Pet("Dog"), Pet("Rabbit")]
def pet_mood(all_pets):
        for i in all_pets:
            i.clock_tick()

while True:
    
    pet_mood(all_pets)
    print('''
    Virtual Pet Game
    ----------------
    1. Adopt Pet
    2. Greet
    3. Teach
    4. Feed
    5. Pet Status
    6. Exit
    ''')

    user_choice = int(input("\nEnter choice: "))
    if (user_choice == 1):

        pet_name = input("Enter your pet's name: ")
        new_pet = Pet(pet_name)
        for i in all_pets:
            if i.name == new_pet.name:
                print("Pet already present!")
                break
            else:
                all_pets.append(new_pet)
                break

    elif (user_choice in (2,3,4,5)):

        print("Choose a pet starting from 0 - {}: {}".format(len(all_pets)-1, [i.name for i in all_pets]))
        pet_choice = int(input("Enter the number of the pet: "))
        chosen_pet = all_pets[pet_choice]
        if (user_choice == 2): chosen_pet.hi()
        elif (user_choice == 3):
            new_word = input("Enter new word to be taught to your pet: ")
            chosen_pet.teach(new_word)
        elif (user_choice == 4): chosen_pet.feed()
        elif (user_choice == 5): chosen_pet.__str__()
        else: print("Invalid Pet!")

    elif (user_choice == 6):

            print("Thank you for playing!")
            break

    else:
        print("Invalid Choice!")





